//
//  PhotoModel.swift
//  Mars Eye
//
//  Created by mohammed abdulla kadib on 3/9/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation
import UIKit

struct PhotoModel {
    
    var photoURL: String
    var earthDate: String
    var cameraName: String
    var cameraFullName: String
    var roverName: String
    var roverLandingDate: String
    var roverLaunchingDate: String
    
    var photoUIImage: UIImage {
        
        var data: Data
        var myImage = UIImage()
        if let url = URL(string: photoURL){
            do {
                data = try Data(contentsOf: url)
                myImage = UIImage(data: data)!
            
            } catch {
                print("error geting photo from url \(error)")
            }
        }
        return myImage
        
    }
    
    
    
    
        
    
    
    
}
