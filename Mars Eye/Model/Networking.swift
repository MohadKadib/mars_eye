//
//  Networking.swift
//  Mars Eye
//
//  Created by mohammed abdulla kadib on 3/9/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation

protocol NetworkingDelegate {
    func didUpdatePhotos(_ networking: Networking,photoModels: [PhotoModel])
    func didFailWithError(error: Error)
}

class Networking {
    
    let nasaURL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&camera=fhaz&api_key=DEMO_KEY#"
    
   
    
    var delegate: NetworkingDelegate?
    
    func fetchPhotos()  {
        
        performRequest(with: nasaURL)
        
        
    }
    
    func performRequest(with urlString: String) {
        
        let url = URL(string: urlString)
        
        let session = URLSession(configuration: .default)
        
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            if error != nil {
                self.delegate?.didFailWithError(error: error!)
                return
            }
            
            if let safeData = data {
                
                if let photoModels = self.parseJSON(safeData){
                    self.delegate?.didUpdatePhotos(self,photoModels: photoModels)
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    func parseJSON(_ photosData: Data) -> [PhotoModel]? {
        
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(PhotosCollection.self, from: photosData)
            var photos: [PhotoModel] = []
            for photo in decodedData.photos {
                let photoModel = PhotoModel(photoURL: photo.img_src, earthDate: photo.earth_date, cameraName: photo.camera.name, cameraFullName: photo.camera.full_name, roverName: photo.rover.name, roverLandingDate: photo.rover.landing_date, roverLaunchingDate: photo.rover.launch_date)
                photos.append(photoModel)
            }
            
            return photos
            
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
        
        
    }
    
    
    
}
