//
//  MarsImageCell.swift
//  Mars Eye
//
//  Created by mohammed abdulla kadib on 3/7/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit

class MarsImageCell: UITableViewCell {
    
    
    @IBOutlet weak var MarsImageView: UIImageView!
    @IBOutlet weak var RoverNameLabel: UILabel!
    @IBOutlet weak var DateImageLabel: UILabel!
    @IBOutlet weak var CameraLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
