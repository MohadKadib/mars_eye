//
//  MarsImageView.swift
//  Mars Eye
//
//  Created by mohammed abdulla kadib on 3/7/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit

class MarsImageView: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    var imageTemp: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = imageTemp
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func InfoButtonClicked(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Done", style: .default) { (action) in
            
        }
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
    
    

}
