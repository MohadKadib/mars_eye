//
//  MarsImagesList.swift
//  Mars Eye
//
//  Created by mohammed abdulla kadib on 3/7/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class MarsImagesList: UIViewController, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var tempImage: UIImage = UIImage(imageLiteralResourceName: "mars")
    
    let disposeBag = DisposeBag()
    
    let urlKey = "https://mars.jpl.nasa.gov/msl-raw-images/proj/msl/redops/ods/surface/sol/01000/opgs/edr/fcam/FLB_486265257EDR_F0481570FHAZ00323M_.JPG"
    
    var networking = Networking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networking.fetchPhotos()
        
        let photosArray = networking.fetchedPhotos?.photos
        
        let objArray: Observable<[Photo]> = Observable.just(photosArray!)
        
        objArray.bind(to: tableView.rx.items(cellIdentifier: "MarsImageCell")) {
           _ ,photo,cell in
            
            if let cellToUse = cell as? MarsImageCell {
                cellToUse.MarsImageView.image = self.convertUrlToImage(imageString: photo.img_src)
                cellToUse.CameraLabel.text = photo.camera.full_name
                cellToUse.DateImageLabel.text = photo.earth_date
                cellToUse.RoverNameLabel.text = photo.rover.name
            }
            
            
        }.disposed(by: disposeBag)
        
        
        
        
        
        
        var data: Data
        if let url = URL(string: urlKey){
            do {
                data = try Data(contentsOf: url)
                tempImage = UIImage(data: data)!
            
            } catch {
                print("error geting photo from url \(error)")
            }
            
            
            
//        tableView.dataSource = self
//        tableView.delegate = self
        
        }
    }

 

//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        print("entered number of rows function")
//        return 1
//
//    }
    
    
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        print("entererd cell for row at function")
//        let cell = tableView.dequeueReusableCell(withIdentifier: "MarsImageCell") as! MarsImageCell
//        var data: Data
//        if let url = URL(string: urlKey){
//            do {
//                data = try Data(contentsOf: url)
//                cell.MarsImageView.image = UIImage(data: data)
//            } catch {
//                print("error geting photo from url \(error)")
//            }
//        }
//
//        return cell
//
//    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let desticnationVC = segue.destination as! MarsImageView

        print("gowa prepare for sague")

        if tableView.indexPathForSelectedRow != nil {

            desticnationVC.imageTemp = tempImage

        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "GoToMarsImageView", sender: self)
        print("inside Sague ya kiwiii")
    }
    
    
    
    func convertUrlToImage (imageString: String) -> UIImage {
        
        var data: Data
        var myImage = UIImage()
        if let url = URL(string: imageString){
            do {
                data = try Data(contentsOf: url)
                myImage = UIImage(data: data)!
            
            } catch {
                print("error geting photo from url \(error)")
            }
        }
        return myImage
    }

}
